package vendingmachine.payment;

import java.awt.event.*;
import javax.swing.*;
import vendingmachine.manager.*;
import vendingmachine.purse.*;

public class PaymentTextFieldManager implements ActionListener {
	Payment payment;
	JTextField jtf;
	PurseTextFieldManager purseTextFieldManager;
	
	public PaymentTextFieldManager(PurseTextFieldManager _ptfm) {
		jtf = new JTextField(5); // making text field have 5 column.
		jtf.addActionListener(this);
		payment = new Payment();
		purseTextFieldManager = _ptfm;
	}
	
	public void actionPerformed(ActionEvent ev) {
		try {
			payment.value = Integer.parseInt(jtf.getText());
			purseTextFieldManager.minusMoney(payment.value);
			jtf.setText("");
		} catch (Exception ex) {
		}
	}
	
	public void registField(ReceptionIOPanelManager riopm) {
		riopm.addComponentGroup(new JLabel("支払い金額"), jtf);
	}
	
	public void reset() {
		payment.value = 0;
		jtf.setText(Integer.toString(payment.value));
	}
	
}
