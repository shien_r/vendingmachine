package vendingmachine.manager;

import javax.swing.*;
import java.awt.GridLayout;

public class ReceptionIOPanelManager {
	JPanel receptionIOPanel;

	public ReceptionIOPanelManager () {
		receptionIOPanel = new JPanel();
		receptionIOPanel.setLayout(new GridLayout(2,4));
	}
	
	public void addComponentGroup(JLabel _jl, JComponent _jcom) {
		JPanel setpanel = new JPanel(); 
		setpanel.add(_jl);
		setpanel.add(_jcom);
		receptionIOPanel.add(setpanel);
	}
	
	public JPanel getReceptionIOPanel() {
		return receptionIOPanel;
	}
}
