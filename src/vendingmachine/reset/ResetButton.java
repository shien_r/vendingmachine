package vendingmachine.reset;

import javax.swing.*;
import java.awt.event.*;
import vendingmachine.purse.*;
import vendingmachine.payment.*;

public class ResetButton implements ActionListener {
	JButton resetButton = new JButton("リセット");
	PurseTextFieldManager pursetfm;
	PaymentTextFieldManager paymenttfm;
	
	public ResetButton () {
		resetButton.addActionListener(this);
	}
	
	public void addPurse(PurseTextFieldManager _pursetfm) {
		pursetfm = _pursetfm;
	}
	
	public void addPayment(PaymentTextFieldManager _paymenttfm) {
		paymenttfm = _paymenttfm;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		pursetfm.reset();
		paymenttfm.reset();
	}
	
	
}
