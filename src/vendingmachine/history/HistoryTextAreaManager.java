package vendingmachine.history;

import javax.swing.*;

import vendingmachine.commodity.Commodity;

public class HistoryTextAreaManager {
	JTextArea jTextArea;
	JScrollPane jsp;
	
	public HistoryTextAreaManager () {
		jTextArea = new JTextArea();
		jTextArea.setColumns(20);
		jsp = new JScrollPane(jTextArea);
		jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	}
	
	public void addBoughtHistory (Commodity _com) {
		jTextArea.insert(_com.getName() + ":" + _com.getPrice().value + "円\n", 0);
	}
	
	public JScrollPane getScrollPane() {
		return jsp;
	}
}
