package vendingmachine.purse;

public class Purse {
	int restMoney;
	int fullRestMoney;
	
	public Purse (int _rmoney) {
		fullRestMoney = restMoney = _rmoney;
	}
	
	public int getRestMoney() {
		return restMoney;
	}
	
	public int minusPrice(int price) {
		restMoney -= price;
		return restMoney;
	}
	
	public void addPrice(int price) {
		restMoney += price;
	}
	
	public void initPurse() {
		restMoney = fullRestMoney;
	}
}
