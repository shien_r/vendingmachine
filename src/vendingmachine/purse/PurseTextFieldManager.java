package vendingmachine.purse;

import javax.swing.*;
import vendingmachine.manager.ReceptionIOPanelManager;
import vendingmachine.showinput.*;

public class PurseTextFieldManager {
	Purse purse;
	JTextField jtf;
	InputMoneyManager inputMoneyManager;
	
	public PurseTextFieldManager(Purse _purse, InputMoneyManager _imm) {
		inputMoneyManager = _imm;
		purse = _purse;
		jtf = new JTextField(Integer.toString(purse.getRestMoney()));
		jtf.setEditable(false);
	}
	
	public void minusMoney(int money) {
		if (purse.getRestMoney() < money)
			return;
		inputMoneyManager.addPrice(money);
		jtf.setText(Integer.toString(purse.minusPrice(money)));
	}
	
	public void getCharge() {

		purse.addPrice(inputMoneyManager.getInputMoney());
		inputMoneyManager.initInputMoney();
		jtf.setText(Integer.toString(purse.getRestMoney()));
	}
	
	public void registField(ReceptionIOPanelManager riopm) {
		riopm.addComponentGroup(new JLabel("財布"), jtf);
	}
	
	public void reset() {
		purse.initPurse();
	}
}
