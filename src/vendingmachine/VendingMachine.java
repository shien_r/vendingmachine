package vendingmachine;

import javax.swing.*;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import vendingmachine.manager.*;
import vendingmachine.charge.ChargeButton;
import vendingmachine.commodity.*;
import vendingmachine.price.*;
import vendingmachine.purse.*;
import vendingmachine.rest.*;
import vendingmachine.showinput.*;
import vendingmachine.payment.*;
import vendingmachine.history.*;

/* panel ごと、text field ごとの interface を作ればよかった？
 * 
 * */

public class VendingMachine extends JFrame {

	private static final long serialVersionUID = 1L;
	String juice_name[] = {
			"コーヒー微糖","コーヒーブラック",
			"デカビタ","オロナミンＣ",
			"ペット水", "ペットさんぴん",
			"お茶", "オレンジジュース", "ドライバーブラック"};
	int juice_price[] = {
			110, 110, 110, 110, 150, 150, 110, 110,110
	};
	
	CommodityButtonManager buttonManagers[];
	Purse purse;
	CommodityLayout commodityLayout[];
	
	final int MONEY_IN_HAND = 10000;
	final int REST = 20;
	
	JLabel titleLabel;
	JPanel allPanel;
	
	public VendingMachine () {
		super();
		purse = new Purse(MONEY_IN_HAND);
		buttonManagers = new CommodityButtonManager[juice_name.length];	
		commodityLayout = new CommodityLayout[juice_name.length];
		allPanel = new JPanel();
		allPanel.setLayout(new GridLayout(3,1));
		add(allPanel);
	}
	
	public void initFrame() {
		this.setLayout(new FlowLayout());
		this.setSize(500,700);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	public Juice createJuice(String name, int price) {
		Juice juice = new Juice();
		juice.setName(name);
		juice.setPrice(new Price(price));
		juice.setRest(new Rest(REST));
		return juice;
	}
	
	public void initCommodityData () {
		JPanel panel = new JPanel();
		InputMoneyManager imm = new InputMoneyManager();
		HistoryTextAreaManager htam = new HistoryTextAreaManager();
		
		panel.setLayout(new GridLayout(3,3));
		for (int i = 0; i < juice_name.length; i++) {
			Juice juice = createJuice(juice_name[i], juice_price[i]);
			
			commodityLayout[i] = new CommodityLayout();
			
			JLabel jlbl = new JLabel(juice.getName());
			commodityLayout[i].addLabel(jlbl);
			
			JRadioButton jrb = new JRadioButton();
			commodityLayout[i].addRadioButton(jrb);

			buttonManagers[i] = new CommodityButtonManager(juice, imm, htam);
			commodityLayout[i].addPriceButton(buttonManagers[i].getButton());
			
			panel.add(commodityLayout[i].finishLayoutConfig());
		}
		allPanel.add(panel);
		initReceptionIO(htam, imm);
	}
	
	public void initReceptionIO(HistoryTextAreaManager htam, InputMoneyManager imm) {
		ReceptionIOPanelManager riopm = new ReceptionIOPanelManager();
		
		Purse purse = new Purse(MONEY_IN_HAND);
		PurseTextFieldManager pursetfm = new PurseTextFieldManager(purse,imm);
		pursetfm.registField(riopm);
		PaymentTextFieldManager paymenttfm 
			= new PaymentTextFieldManager(pursetfm);
		paymenttfm.registField(riopm);
		ChargeButton cb = new ChargeButton(pursetfm);
		cb.registField(riopm);
		imm.registField(riopm);
		
		allPanel.add(riopm.getReceptionIOPanel());
		allPanel.add(htam.getScrollPane());
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		VendingMachine vm = new VendingMachine();
		vm.initCommodityData();
		vm.initFrame();
		
		vm.setTitle("自動販売機");
		
	}
}
