package vendingmachine.coin;

import vendingmachine.price.Price;

public class OneHundredYen implements Coin {
	
	private Price price;
	private final int ONE_HUNDRED_YEN = 100;
	
	public OneHundredYen() {
		price = new Price(ONE_HUNDRED_YEN);
	}
	
	public Price getCoinPrice() {
		return price;
	}

}
