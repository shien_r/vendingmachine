package vendingmachine.coin;

import vendingmachine.price.Price;

public interface Coin {
	public Price getCoinPrice();
}
