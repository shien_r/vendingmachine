package vendingmachine.coin;

import vendingmachine.price.Price;

public class TenYen implements Coin {

	private Price price;
	private final int TEN_YEN = 10;
	
	public TenYen() {
		price = new Price(TEN_YEN);
	}
	
	public Price getCoinPrice() {
		return price;
	}
}
