package vendingmachine.coin;

import vendingmachine.price.Price;

public class FiftyYen implements Coin {
	private Price price;
	private final int FIFTY_YEN = 50;
	
	public FiftyYen() {
		price = new Price(FIFTY_YEN);
	}
	
	public Price getCoinPrice() {
		return price;
	}
}
