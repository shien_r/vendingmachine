package vendingmachine.showinput;

import javax.swing.*;

import vendingmachine.manager.ReceptionIOPanelManager;
import vendingmachine.price.Price;

public class InputMoneyManager {
	JTextField jtf;
	InputMoney inputMoney;
	
	public InputMoneyManager () {
		inputMoney = new InputMoney();
		jtf = new JTextField();
		jtf.setColumns(5);
		jtf.setText("0");
		jtf.setEditable(false);
	}
	
	public void addPrice(int _inputmoney) {
		inputMoney.value = Integer.parseInt(jtf.getText());
		inputMoney.value += _inputmoney;
		jtf.setText(Integer.toString(inputMoney.value));
	}
	
	public void minusPrice(Price _price) {
		if (_price.value < inputMoney.value) {
			inputMoney.value = Integer.parseInt(jtf.getText());
			inputMoney.value -= _price.value;
			jtf.setText(Integer.toString(inputMoney.value));
			return;
		}
	}
	
	public int getInputMoney() {
		return inputMoney.value;
	}
	
	public void initInputMoney() {
		inputMoney.value = 0;
		jtf.setText("0");
	}
	
	public void registField(ReceptionIOPanelManager riopm) {
		riopm.addComponentGroup(new JLabel("支払い"), jtf);
	}
}
