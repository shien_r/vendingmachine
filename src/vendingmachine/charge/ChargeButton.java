package vendingmachine.charge;

import javax.swing.*;

import java.awt.event.*;
import vendingmachine.manager.*;
import vendingmachine.purse.*;

public class ChargeButton implements ActionListener {
	JButton chargeButton;
	PurseTextFieldManager purseTextFieldManager;
	
	public ChargeButton (PurseTextFieldManager _ptfm) {
		chargeButton = new JButton("お釣り");
		chargeButton.addActionListener(this);
		purseTextFieldManager = _ptfm;
	}

	public void registField(ReceptionIOPanelManager riopm) {
		riopm.addComponentGroup(new JLabel(""), chargeButton);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		purseTextFieldManager.getCharge();
	}
}
