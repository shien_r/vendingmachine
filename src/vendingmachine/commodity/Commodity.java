package vendingmachine.commodity;

import vendingmachine.price.Price;
import vendingmachine.rest.Rest;

public interface Commodity {
	public String getName();
	public void setName(String _name);
	public Price getPrice();
	public void setPrice(Price _price);
	public Rest getRest();
	public void setRest(Rest _rest);
	public void declementRest();
}
