package vendingmachine.commodity;

import java.awt.Color;
import javax.swing.*;
import javax.swing.border.*;

public class CommodityLayout {
	JPanel panel;
	GroupLayout groupLayout;
	GroupLayout.SequentialGroup hsgroup;
	GroupLayout.SequentialGroup vsgroup;

	GroupLayout.ParallelGroup hLblBtnGroup;
	GroupLayout.ParallelGroup vRdbtnBtnGroup;
	GroupLayout.ParallelGroup vpgroup;
	LineBorder border = new LineBorder(Color.BLACK, 1, true);

	public CommodityLayout() {
		panel = new JPanel();
		panel.setBorder(border);
		groupLayout = new GroupLayout(panel);
		panel.setBackground(Color.WHITE);
		panel.setLayout(groupLayout);
		vsgroup = groupLayout.createSequentialGroup();
		hsgroup = groupLayout.createSequentialGroup();
		hLblBtnGroup = groupLayout.createParallelGroup();
		vRdbtnBtnGroup = groupLayout.createParallelGroup();
		groupLayout.setAutoCreateContainerGaps(true);
		groupLayout.setAutoCreateGaps(true);
	} 

	public void addRadioButton(JRadioButton _jrb) {
		panel.add(_jrb);
		hsgroup.addGroup(groupLayout.createParallelGroup()
				.addComponent(_jrb));
		vRdbtnBtnGroup.addComponent(_jrb);

		vsgroup.addGroup(groupLayout.createParallelGroup());
	}

	public void addPriceButton(JButton _jb) {
		panel.add(_jb);
		hLblBtnGroup.addComponent(_jb);
		vRdbtnBtnGroup.addComponent(_jb);
	}

	public void addLabel(JLabel _jlbl) {

		_jlbl.setBorder(border);
		panel.add(_jlbl);
		vsgroup.addGroup(groupLayout.createParallelGroup()
				.addComponent(_jlbl));
		hLblBtnGroup.addComponent(_jlbl);
	}

	public JPanel finishLayoutConfig() {
		hsgroup.addGroup(hLblBtnGroup);
		vsgroup.addGroup(vRdbtnBtnGroup);
		groupLayout.setHorizontalGroup(hsgroup);
		groupLayout.setVerticalGroup(vsgroup);
		return panel;
	}
}
