package vendingmachine.commodity;

import javax.swing.*;
import java.awt.event.*;
import vendingmachine.showinput.*;
import vendingmachine.history.*;

public class CommodityButtonManager implements ActionListener {
	JButton jbutton;
	InputMoneyManager inputMoneyManager;
	Commodity commodity;
	HistoryTextAreaManager historyTextAreaManager;
	
	
	public CommodityButtonManager(Commodity _com, InputMoneyManager _imm, HistoryTextAreaManager _htam) {
		commodity = _com;
		inputMoneyManager = _imm;
		historyTextAreaManager = _htam;
		jbutton = new JButton(_com.getPrice().value + "円");
		jbutton.addActionListener(this);
		
	}
	
	public JButton getButton() {
		return jbutton;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		commodity.declementRest();
		historyTextAreaManager.addBoughtHistory(commodity);
		inputMoneyManager.minusPrice(commodity.getPrice());
		// TODO Auto-generated method stub
		
	}
}
