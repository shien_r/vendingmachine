package vendingmachine.commodity;

import vendingmachine.price.Price;
import vendingmachine.rest.Rest;

public class Juice implements Commodity {

	private Price price;
	private String name;
	private Rest rest;
	
	public void setName(String _name) {
		name = _name;
	}
	public String getName() {
		return name;
	}
	public void setPrice(Price _price) {
		price = _price;
	}
	
	public Price getPrice() {
		return price;
	}
	
	public Rest getRest() {
		return rest;
	}
	
	public void declementRest() {
		rest.value--;
	}

	public void setRest(Rest _rest) {
		rest = _rest;
	}
	
}
